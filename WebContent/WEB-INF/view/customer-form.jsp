<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!doctype html>
<html>

<head>
	<title>Save Customer</title>
	<link type ="text/css" 
			rel ="stylesheet" 
			href="${pageContext.request.contextPath}/resources/css/style.css">
			
	<link type ="text/css" 
			rel ="stylesheet" 
			href="${pageContext.request.contextPath}/resources/css/add-customer-style.css">
</head>


<body>

	<div id="wrapper">
		<div id="header">
			<h2>CRM Manager</h2>
		</div>
	</div>

	<div id="container">
		<h3>Save Customer</h3>
		
		<form:form action="saveCustomer" modelAttribute="customer" method="POST">
		
			<table>
				<tbody>
					<tr>
						<td><label>First name</label></td>
						<td><form:input path="firstName"/></td>
				
					</tr>
					
				<tr>
						<td><label>Last name</label></td>
						<td><form:input path="lastName"/></td>
				
					</tr>
				<tr>
						<td><label>email</label></td>
						<td><form:input path="email"/></td>
				
					</tr>
				</tbody>
			</table>
				<tr>
						<td><label></label></td>
						<td><input type="submit" value="save" class="save"/></td>
					</tr>
		</form:form>
		
		<div style = "clear; both;"></div>
		
		<p>
			<a href="${pageContext.request.contextPath}/customer/list">Back to List</a> 
		</p>
		
	</div>

</body>

</html>